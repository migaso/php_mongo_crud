<?php 
require_once __DIR__ . "/vendor/autoload.php"; 
    function obtenerBaseDeDatos()
    {
        $host = "10.10.11.106";
        $puerto = "27017";
        $usuario = "Master"; 
        $pass = "12345"; 
        $nombreBD = "Nominas";
        # Crea algo como mongodb://migue:hunter@127.0.0.1:27017/agenda
        $cadenaConexion = sprintf("mongodb://%s:%s@%s:%s/%s", $usuario, $pass, $host, $puerto, $nombreBD);
        $cliente = new MongoDB\Client($cadenaConexion);
        // echo $cliente;
        return $cliente->selectDatabase($nombreBD);
    }
//CRUD
    function getInfo($conexion){
        try{
            return $conexion -> find();
        } catch (Exception $e) {
            echo "\n\n Error al leer los datos de BDs: ",$e->getMessage(),"\n" ;
            return false;
        }
    }
    function insertInfo($conexion,$data){
        $conexion -> insert($data);
    }
    function updateInfo($conexion,$data,$id){
        $conexion -> update(
            ["_id" => new MongoDB\BSON\ObjectId($id)],
            [ '$set' =>[ $data ],
                //     "campo" => "valor actualizado",
                //     "otro campo" => "otro valor actualizado",
            ]
        );
    }
    function deleteInfo($conexion,$id){
       return $conexion -> deleteOne(["_id" => new MongoDB\BSON\ObjectId($id)]);
    }

$baseDeDatos= obtenerBaseDeDatos();
$coleccion = $baseDeDatos->Usuario;
$cursor = getInfo($coleccion);
// $resultado = deleteInfo($coleccion,'5e1e510135c6d82e30858daa');
// $eliminados = $resultado->getDeletedCount();

if($cursor){
    foreach ($cursor as $dato){
        if( strlen($dato->nombre) > 0 )
        echo '<br>'.$dato->nombre.'<br>';
    }
}

// echo $eliminados;

// var_dump($cursor);
?>