#####
CRUD mongoDB y PHP
#####

1. instalar las dependencias: 
    - Servicio Apache y PHP
    - Extensión para PHP de mongoDB (revisar la versión php), descargar archivo .dll
    - Instalar Composer 

2. Ejecutar :
    - Servidor XAMP, LAMP, WAMP... 
    - Dentro de la carpeta de proyecto en CLI : "composer require mongodb/mongodb" 

3. Revisar:
    - info.php (buscar extension mongodb), si no existe realizar paso 1 y 2 nuevamente.

Se utilizó PHP 7.3.11 en XAMPP como entorno de pruebas.